import { createBrowserHistory } from "history";
const dotenv = require('dotenv')
dotenv.config()
//On redéclare ensuite les méthodes
export const history = createBrowserHistory();


export const formatUserName = (firstName:string,lastName:string) => {
    let formatedB0arderName: string = "" 
    if(firstName && lastName){
      formatedB0arderName = firstName[0].toUpperCase() + firstName.slice(1)+ " "+ lastName.substring(0, 1).toUpperCase() + "."
    }else if (!firstName && lastName){
      formatedB0arderName = lastName[0].toUpperCase() + lastName.slice(1)
    }else if (firstName && !lastName){
      formatedB0arderName = firstName[0].toUpperCase() + firstName.slice(1)
    }		
    return formatedB0arderName
  }
  
export const getRole= () => {
    const me_str = localStorage.getItem('user_me') ? localStorage.getItem('user_me') : ""
    return (me_str ? JSON.parse(me_str).userRole.name : "none")
}

export const getMeInfos= () => {
    const me_str = localStorage.getItem('user_me') ? localStorage.getItem('user_me') : ""
    return (me_str ? JSON.parse(me_str) : null)
}

export const isDev = () => {
    const development: boolean = (process.env.REACT_APP_ENV && process.env.REACT_APP_ENV === 'development') ? true : false;
    return development
}

export const ressourcePath = isDev() ? (process.env.REACT_APP_BACKEND_ADRESS_DEV+":"+process.env.REACT_APP_BACKEND_PIC_PORT_DEV+"/files") : (process.env.REACT_APP_BACKEND_ADRESS+":"+process.env.REACT_APP_BACKEND_PIC_PORT+"/files")
export const apiPath = isDev() ? (process.env.REACT_APP_BACKEND_ADRESS_DEV+":"+process.env.REACT_APP_BACKEND_PORT_DEV) : (process.env.REACT_APP_BACKEND_ADRESS+":"+process.env.REACT_APP_BACKEND_PORT)

