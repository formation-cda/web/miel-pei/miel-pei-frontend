import gql from "graphql-tag";

export const GET_ROLE_QUERY = gql`
	query getRole(
            $id:String!){
        getRole (
            id: $id          
        ){
            user {
                userRole {
                    id
                    name
                }
            }
            success
            message
        }
	}
`;
