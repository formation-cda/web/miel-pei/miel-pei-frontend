import gql from "graphql-tag";

export const LOGIN_MMUTATION = gql`
    mutation loginUser(
                $email:String!
                $password: String!){
        loginUser (
            email: $email
            password: $password            
        ){
            token,  
            user {
                id
                lastName
                firstName
                email            
                userRole {
                    id
                    name
                }
            }
            success,
            message
        }
    }
`;

export const LOG_OUT_MUTATION = gql`
	mutation logout {
		logout {
            success
            message
        }
	}
`;

export const REGISTER_MMUTATION = gql`
    mutation signupUser(
                $email:String!
                $lastName: String!
                $firstName:String!
                $password: String!
                $role: String!){
        signupUser (
            email: $email
            lastName: $lastName  
            firstName: $firstName
            role: $role
            password: $password           
        ){
            token,  
            user {
                id
                lastName
                firstName
                email           
                userRole {
                    id
                    name
                }
            }
            success,
            message
        }
    }
`;