import gql from "graphql-tag";

export const QUERY_PRODUCTS = gql`
    query products{
        products {
            data {
                id  
                name
                price
                pictures
                qtyInStock
                qtyInCart
                description
                exploitation {
                    id
                    name
                }
            }
            success
            message
        }       
    }
`;

export const QUERY_ALL_MY_PRODUCTS = gql`
    query allMyproducts{
        allMyproducts {
            data {
                id  
                name
                price
                pictures
                qtyInStock
                qtyInCart
                description
                exploitation {
                    id
                    name
                }
            }
            success
            message
        }       
    }
`;

export const QUERY_ALL_PRODUCTS_BY_PRODUCER = gql`
    query allProductsByProducer($id:String!){
        allProductsByProducer (id: $id){
            data {
                id  
                name
                price
                pictures
                qtyInStock
                qtyInCart
                description
                exploitation {
                    id
                    name
                }
            }
            success
            message
        }       
    }
`;

export const QUERY_PRODUCT = gql`
    query product($id:String!){
        product (id: $id){
            data {
                id  
                name
                price
                pictures
                qtyInStock
                qtyInCart
                description
                exploitation {
                    id
                    name
                }
            }
            success
            message
        }       
    }
`;