import gql from "graphql-tag";

export const ADD_PRODUCT_MMUTATION = gql`
    mutation addProduct(
                $name: String!
                $price: String!
                $qty_in_stock: String!
                $pictures: Upload!
                $description: String!
                $exploitationId: String!
                ){
        addProduct (
            name: $name
            price: $price
            qty_in_stock: $qty_in_stock
            pictures: $pictures
            description: $description
            exploitation: $exploitationId
        ){
            success,
            message
        }
    }
`;   

export const DELETE_PRODUCT_MMUTATION = gql`
    mutation deleteProduct(
                $id: String!
                ){
        deleteProduct (
            id: $id    
        ){
            success,
            message
        }
    }
`;