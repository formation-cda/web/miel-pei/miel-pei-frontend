import { makeStyles } from "@material-ui/core/styles";

export const useCommonStyles = makeStyles((theme) => ({    
  button: {
    color: '#ffffff',
  },
}));