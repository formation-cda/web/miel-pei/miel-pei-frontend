import axios from "axios";
import { apiPath, isDev } from "../utils/utilities";

export const fetchAddress = (str:string,callback:any) => {
  let url= `https://api-adresse.data.gouv.fr/search/?q=${str}&type=&autocomplete=1&limit=10`
  axios
    .get(url)
    .then((response) => response.data)
    .then((res) => {
      callback(res);
    })
    .catch((error) => {
      console.log(error);

      callback('error occurred');
    });
}
// export const fetchData = (body:any,callback:any) => {
//   let url= api_url
//   axios
//     .get(url, body)
//     .then((response) => response.data)
//     .then((res) => {
//       callback(res);
//     })
//     .catch((error) => {
//       console.log(error);

//       callback('error occurred');
//     });
// }