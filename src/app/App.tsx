import React, { useState } from 'react';
// import logo from '../logo.svg';
// import { Counter } from '../features/counter/Counter';
import {Login} from '../components/login/Login'
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from '../components/home/Home';
import Dashboard from '../components/dashboard/Dashboard';

import { ApolloClient, createHttpLink, InMemoryCache, ApolloProvider, split } from '@apollo/client';
import { createUploadLink } from "apollo-upload-client";
import { setContext } from '@apollo/client/link/context';
import ProtectedRoute from '../components/ProtectedRoote';
import { Register } from '../components/register/Register';
import { ForgotPassword } from '../components/forgot-password/ForgotPassword';
import { NewPassword } from '../components/new-password/NewPassword';
import { Footer } from '../components/footer/Footer';
import { Header } from '../components/header/Header';
import DashboardAdmin from '../components/dashboardAdmin/DashboardAdmin';
import { getRole } from '../utils/utilities';
import ManageUser from '../components/manageUser/ManageUser';
import { Exploitations } from '../components/exploitation/Exploitations';
import { Products } from '../components/product/Products';
import { Cart } from '../components/cart/Cart';
import Checkout from '../components/Order/Checkout';
import ProducerSheet from '../components/producerSheet/ProducerSheet';
import ProductSheet from '../components/productSheet/ProductSheet';
import ExploitationSheet from '../components/exploitationSheet/ExploitationSheet';

console.log("ENV :",process.env)
const upLoadLink = createUploadLink({
  //uri: "https://pacific-brushlands-41731.herokuapp.com/",
  uri: (process.env.REACT_APP_ENV === 'development') ? (process.env.REACT_APP_BACKEND_ADRESS_DEV+":"+process.env.REACT_APP_BACKEND_PORT_DEV) : (process.env.REACT_APP_BACKEND_ADRESS+":"+process.env.REACT_APP_BACKEND_PORT)
  //credentials: "include",
  /*fetchOptions: {
    mode: 'no-cors',
  },*/
});

const httpLink = createHttpLink({
  uri: (process.env.REACT_APP_ENV === 'development') ? (process.env.REACT_APP_BACKEND_ADRESS_DEV+":"+process.env.REACT_APP_BACKEND_PORT_DEV) : (process.env.REACT_APP_BACKEND_ADRESS+":"+process.env.REACT_APP_BACKEND_PORT)
});


upLoadLink.concat(httpLink)


const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(upLoadLink),
  cache: new InMemoryCache()
});

function App() { 
  const role = getRole()

  return (
    <ApolloProvider client={client}>     
      <Header />     
      <div id="mainContentApp">
        <Router>
          <Route exact path='/' component={Home} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/register' component={Register} />
          <Route exact path='/forgot-password' component={ForgotPassword} />
          <Route exact path='/new-password' component={NewPassword} />
          <Route exact path='/producer-sheet/:producerId' component={ProducerSheet} />
          <Route exact path='/product-sheet/:productId' component={ProductSheet} />
          <Route exact path='/exploitation-sheet/:exploitationId' component={ExploitationSheet} />
          <Route exact path='/cart' component={Cart} />
          {/* <Route exact path='/order' component={CustomerOrder} /> */}
          {role === "client" &&
            <>
              <ProtectedRoute exact path='/dashboard' component={Dashboard} />
              {/* <ProtectedRoute exact path='/manage-stock' component={ManageStock} />*/}
              <ProtectedRoute exact path='/checkout' component={Checkout} />
             {/*}  <ProtectedRoute exact path='/order-history' component={OrderHistory} />
              <ProtectedRoute exact path='/profil' component={Profil} /> */}
            </>
          }
          {role === "producer" &&
            <>
              <ProtectedRoute exact path='/dashboard' component={Dashboard} />
              {/* <ProtectedRoute exact path='/manage-stock' component={ManageStock} />*/}
              <ProtectedRoute exact path='/exploitation' component={Exploitations} />
              <ProtectedRoute exact path='/product' component={Products} />
              <ProtectedRoute exact path='/checkout' component={Checkout} />
             {/*}  <ProtectedRoute exact path='/order-history' component={OrderHistory} />
              <ProtectedRoute exact path='/profil' component={Profil} /> */}
            </>
          }
          {role === "admin" &&
            <>
              <ProtectedRoute exact path='/dashboard' component={DashboardAdmin} />
              <ProtectedRoute exact path='/manage-user' component={ManageUser} />
              {/* <ProtectedRoute exact path='/profil' component={Profil} /> */}
            </>
          }
          
        </Router>
      </div>
      <Footer />
    </ApolloProvider> 
  );
}

export default App;
