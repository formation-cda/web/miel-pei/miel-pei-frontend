import React, { useState } from 'react'
// import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { useMutation } from '@apollo/client';
import {LOGIN_MMUTATION} from '../../graphql/user/mutation/userMutation'
import { Avatar, Container, CssBaseline, Grid, Link, TextField, Typography } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {useStyles} from './loginStyle'
import {history} from '../../utils/utilities'
import {useCommonStyles} from '../../common/commonStyle'

export const Login = () => {
    const classes = useStyles()
    const commonClasses = useCommonStyles()
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const [loginUser, {loading, error, data}] = useMutation(LOGIN_MMUTATION)
    
    if(data && data.loginUser.success) {
        console.log(data)
        localStorage.setItem('token', data?.loginUser?.token)
        localStorage.setItem('user_me',JSON.stringify(data?.loginUser?.user))
        history.push('/dashboard')
        history.go(0)
    }
    return (
            <Container component="main" maxWidth="xs" className={classes.main}>
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                    S'identifier
                    </Typography>
                    <form className={classes.form}
                        onSubmit={async e => {
                            e.preventDefault();
                            //  appel de la methode de mutation
                            loginUser({
                                variables:{
                                    email: email,
                                    password: password
                                }
                            })
                        }}
                    >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Adresse email"
                            name="email"
                            autoComplete="email"
                            onChange={
                                (e)=>{
                                    setEmail(e.target.value)
                                }
                            }
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Mots de passe"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={
                                (e)=>{
                                    setPassword(e.target.value)
                                }
                            }
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit+" "+commonClasses.button}
                        >
                            Valider
                        </Button>
                        <Grid container>
                            <Grid item xs>
                            <Link href="forgot-password" variant="body2">
                                Mot de passe oublié?
                            </Link>
                            </Grid>
                            <Grid item>
                            <Link href="/register" variant="body2">
                                {"Vous n'avez pas de compte? S'inscrire"}
                            </Link>
                            </Grid>
                        </Grid>
                        <Grid container justify="center">
                            
                                {/* {loading && <p>En cours...</p>} */}                                
                                {error && <p className={classes.error}>
                                    <Typography>
                                        Une erreur s'est produit lors de la connexion
                                    </Typography>
                                </p>}
                                <p className={classes.error}>
                                    <Typography>
                                     {!data?.loginUser?.success ? data?.loginUser?.message : ''}
                                    </Typography>
                                </p>
                            
                        </Grid>
                    </form>
                </div>
            </Container>
        )
}