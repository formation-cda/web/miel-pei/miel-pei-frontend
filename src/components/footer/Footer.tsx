
import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container';
import {useStyles} from './footerStyle'

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://www.linkedin.com/in/c%C3%A9drick-baro-a00b181a0/">
            Cédrick | 2021
        </Link>{' '}
        {/* {new Date().getFullYear()}
        {'.'} */}
        </Typography>
    );
}

export const Footer = ()=>{
    const classes = useStyles();
    return (

    <div className={classes.root}>
        <footer className={classes.footer}>
            <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
            Miel Péi
            </Typography>
            <Copyright />
        </footer>
    </div>
    )
}
