import { useMutation } from '@apollo/client';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControlLabel, Checkbox, Button, Link, Box, Switch } from '@material-ui/core';
import { Copyright } from '@material-ui/icons';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import React, { useState } from 'react'
import { REGISTER_MMUTATION } from '../../graphql/user/mutation/userMutation';
import {useStyles} from './registerStyle'
import {history} from '../../utils/utilities'
import {useCommonStyles} from '../../common/commonStyle'

export const Register = () => {
    const classes = useStyles()
    const commonClasses = useCommonStyles()
    const [lastName,setLastName] = useState('')
    const [firstName,setFirstName] = useState('')
    const [email,setEmail] = useState('')
    const [checked,setChecked] = useState(false)
    const [password,setPassword] = useState('')
    const [confirmPassword,setConfirmPassword] = useState('')
    const [errorMessage,setErrorMessage] = useState('')
    const [signupUser, {loading, error, data}] = useMutation(REGISTER_MMUTATION)
    
    if(data) {
        localStorage.setItem('token', data?.signupUser?.token)
        localStorage.setItem('user_me',JSON.stringify(data?.signupUser?.user))
        history.push('/dashboard')
        history.go(0)
    }
    const handleChangeSwitch = (event:any) => {
        setChecked(event.target.checked);
    };

    return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                    S'inscrire
                    </Typography>
                    <form className={classes.form} 
                        onSubmit={async e => {
                            e.preventDefault();
                            //  appel de la methode de mutation
                            if(password !== confirmPassword) {
                                setErrorMessage("Mots de passe non identique")
                                setTimeout(()=>setErrorMessage(""),2000)
                            } else {
                                signupUser({
                                    variables:{
                                        lastName: lastName,
                                        firstName: firstName,
                                        email: email,
                                        password: password,
                                        role: checked ? "producer" : "client"
                                    }
                                })
                            }
                        }}
                    >
                         <FormControlLabel
                            control={
                            <Switch
                                checked={checked}
                                onChange={handleChangeSwitch}
                                name="checked"
                                color="primary"
                            />
                            }
                            label="Producteur"
                        />
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Nom"
                                    name="lastName"
                                    autoComplete="lname"
                                    onChange={
                                        (e)=>{
                                            setLastName(e.target.value)
                                        }
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="fname"
                                    name="firstName"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="Prénom"
                                    onChange={
                                        (e)=>{
                                            setFirstName(e.target.value)
                                        }
                                    }
                                    autoFocus
                                />                            
                            </Grid>
                            <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label="Adresse email"
                                name="email"
                                onChange={
                                    (e)=>{
                                        setEmail(e.target.value)
                                    }
                                }
                                autoComplete="email"
                            />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Mots de passe"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    onChange={
                                        (e)=>{
                                            setPassword(e.target.value)
                                        }
                                    }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Confirmation de mots de passe"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    onChange={
                                        (e)=>{
                                            setConfirmPassword(e.target.value)
                                        }
                                    }
                                />
                            </Grid>
                            {/* <Grid item xs={12}>
                            <FormControlLabel
                                control={<Checkbox value="allowExtraEmails" color="primary" />}
                                label="I want to receive inspiration, marketing promotions and updates via email."
                            />
                            </Grid> */}
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit+" "+commonClasses.button}
                        >
                            Valider
                        </Button>
                        <Grid container justify="flex-end">
                            <Grid item>
                            <Link href="/login" variant="body2">
                                Vous avez déjà un compte? S'identifier
                            </Link>
                            </Grid>
                        </Grid>
                        <Grid container justify="center">
                            <Typography>
                                {errorMessage !== "" && <p className={classes.error}>{errorMessage}</p>}
                                {error && <p className={classes.error}>Une erreur s'est produit lors de la connexion</p>}
                                <p className={classes.error}>{!data?.signupUser?.success ? data?.signupUser?.message : ''}</p>
                            </Typography>
                        </Grid>
                    </form>
                </div>
            </Container>
        )        
}