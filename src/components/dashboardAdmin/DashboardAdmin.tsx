import React from 'react';
import { Typography, Container, Grid } from '@material-ui/core';
import { useStyles } from './dashboardAdminStyle';
import { useQuery } from '@apollo/client';
import { QUERY_PRODUCTS } from '../../graphql/products/query/productQuery';
import { formatUserName, getMeInfos } from '../../utils/utilities';

const DashboardAdmin = () => {
  const classes = useStyles();
  const user:any = getMeInfos()
  return (
    <main className={classes.main}>
      {/* Hero unit */}
      <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
            Tableau de bord
          </Typography>
          <Typography variant="h6" align="center" color="textSecondary" paragraph>
            Bienvenu dans votre tableau de bord Mr./Mme {(user.firstName && user.firstName !== "") ? formatUserName(user.firstName,user.lastName) : user.lastName}
          </Typography>
        </Container>
      </div>
    </main>
  )
};

export default DashboardAdmin;