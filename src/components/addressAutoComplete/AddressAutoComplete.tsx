
import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { fetchAddress } from '../../services/axiosService'
import {throttle} from 'lodash';
import { PinDropSharp } from '@material-ui/icons';

const AddressAutoComplete = (props:any) => {
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState<any>([]);
  const [optionUpdated, setOptionUpdated] = React.useState(0);
  const [inputValue, setInputValue] = React.useState('');
  const loading = open && options.length === 0;

  const handleFetchData = async (str: string) => {
    if(str) {
      await fetchAddress(str,async (data:any)=>{
        const optionsLocal: any = []
        if(data?.features && data?.features.length > 0) {
            data?.features?.map((elem:any) => {
                if(elem.properties.label !== "Rue" || elem.properties.label !== "rue") {
                    optionsLocal.push({value: JSON.stringify({lat:elem.geometry?.coordinates[1], lng:elem.geometry?.coordinates[0]}), label: elem.properties.label})
                }                        
            })                     
        }        
          await setOptions(optionsLocal);
          await setOptionUpdated(Date.now)
          console.log('Response options:', options)
      })     
    }
  }

  const handleFetchDataThrottled =  throttle(handleFetchData, 2000) // Throttle (let recall the function after 2000 seconds min)

  const handleAddress = (selectedAddress: any) => {
    if(selectedAddress) {
      const objAdress = {lat: JSON.parse(selectedAddress.value).lat, lng: JSON.parse(selectedAddress.value).lng, address: selectedAddress.label}
      props.handleAddress(objAdress)
    }
  }
  React.useEffect(() => {

    console.log('Response options IN USEEFFECT :', options)

    if (!open) {
      setOptions([]);
    }
    if (!loading) {
      return undefined;
    }

  }, [open, optionUpdated]);

  return (
    <Autocomplete
      id="asynchronous-autocomplete"
      // style={{ width: 300 }}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      getOptionSelected={(option:any, value:any) => option.label === value.label}
      getOptionLabel={(option:any) => option.label}
      options={options}
      loading={loading}
      onInputChange={async (event, newInputValue) => {
        await handleFetchData(newInputValue)
      }}
      onChange={async (event, selectedAddress) => {
        console.log("VALUE in ONCHANGE:", selectedAddress)
        handleAddress(selectedAddress)
      }}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Adresse"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}
export default AddressAutoComplete