import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControlLabel, Checkbox, Button, Link, Box } from '@material-ui/core';
import { Copyright } from '@material-ui/icons';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import React from 'react'
import {useStyles} from './forgotPasswordStyle'
import {useCommonStyles} from '../../common/commonStyle'

export const ForgotPassword = () => {
    const classes = useStyles()
    const commonClasses = useCommonStyles()
    return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                    <AccountCircleOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                    Mots de passe Oublié
                    </Typography>
                    <form className={classes.form} noValidate>
                    <Grid container spacing={2}>                        
                        <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                        />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit+" "+commonClasses.button}
                    >
                        Valider
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                        <Link href="/login" variant="body2">
                            S'identifier
                        </Link>
                        </Grid>
                    </Grid>
                    </form>
                </div>
            </Container>
        )        
}