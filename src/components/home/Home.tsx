import React, { useEffect, useState } from 'react';
import { Typography, Container, Grid, CardActions, Button } from '@material-ui/core';
import { useStyles } from './homeStyle';
import { useQuery } from '@apollo/client';
import { QUERY_PRODUCTS } from '../../graphql/products/query/productQuery';
import {ProductCard} from '../card/Card';
import Map from '../map/Map'
import { getMeInfos } from '../../utils/utilities';
import { QUERY_EXPLOITATIONS } from '../../graphql/exploitation/query/exploitationQuery';

interface IExploitationList {
  data: IExploitation[]
}
interface IExploitation {
  address: string,
  id: string,
  name: string,
  description:string,
  pictures: string
  lat: string,
  lng: string
}

const Home = () => {
  const classes = useStyles();
  const [expLoaded, setExploaded] = useState(false)
  const [showMore, setShowMore] = useState(false)
  const [exploitationsList, setExploitationsList] = useState <IExploitationList | null>(null);
  const {loading, error, data} = useQuery(QUERY_PRODUCTS)   
  const {data: dataE, loading: loadingE, error: erorE} = useQuery(QUERY_EXPLOITATIONS)  
  const handleLoadExploitation = async (dataE:any) => {
    //    await setTimeout(async ()=>{
        await setExploitationsList({...exploitationsList, data: dataE?.exploitations?.data})       
        setExploaded(true)    
    // },3000)
    console.log("ExploitationList  DATAE:",dataE)
    console.log("ExploitationList :",exploitationsList)
  };

  const user:any = getMeInfos()
  if(loading)<p>Loading...</p>
  if(data){
    console.log("Data :",data)
  }

  console.log("Data222 :",data)
  if(dataE){
    if(!expLoaded) {
      setTimeout(async ()=>{
        await handleLoadExploitation(dataE)
      },3000)
    }    
    console.log("DATAE : ",dataE)
  }
  return (
    <main className={classes.main}>
      {/* Hero unit */}
      <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
            Miel Péi
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary" paragraph>
            Bienvenu sur notre site dédié au vente de miel en ligne de nos producteurs locaux.
          </Typography>
        </Container>
      </div>
      <Container className={classes.cardGrid} maxWidth="md">
        {/* End hero unit */}
        {data && data.products.success && data.products.data.length <= 6 &&
          <Grid container spacing={4}>
            {data && data.products.success &&
              data.products.data.map((product:any) => (
                <Grid item key={product.id} xs={12} sm={6} md={4}>
                  <ProductCard data = {product} />
                </Grid>
              ))
            }            
          </Grid>
        }
        {data && data.products.success && data.products.data.length > 6 && !showMore &&
          <>
            <Grid container spacing={4}>
              {data && data.products.success &&
                data.products.data.map((product:any, index: number) => {
                  if(index < 6) {
                    return (
                      <Grid item key={product.id} xs={12} sm={6} md={4}>
                        <ProductCard data = {product} />
                      </Grid>
                    )}
                }
                )                
              }
            </Grid>
            <Grid>
            <CardActions style={{justifyContent:'center',marginTop: 50}}>
                <Button size="small" color="primary" onClick={()=>{setShowMore(true)}}>
                  Voir plus
                </Button>
                {/* <Button size="small" color="primary">
                  Ajouter au panier
                </Button> */}
              </CardActions>
            </Grid>
          </>
        }
        {data && data.products.success && data.products.data.length > 6 && showMore &&
          <>
            <Grid container spacing={4}>
              {data && data.products.success && data.products.data.length > 6 &&
                data.products.data.map((product:any) => (
                  <Grid item key={product.id} xs={12} sm={6} md={4}>
                    <ProductCard data = {product} />
                  </Grid>
                ))
              }
              </Grid>
              <Grid>
              <CardActions style={{justifyContent:'center'}}>
                <Button size="small" color="primary" onClick={()=>{setShowMore(false)}}>
                  Voir moins
                </Button>
                {/* <Button size="small" color="primary">
                  Ajouter au panier
                </Button> */}
              </CardActions>
            </Grid>
          </>
        }
      </Container>
      {/* Start Map section */}
      <Map exploitations={exploitationsList}/>
      {/* End Map section */}
    </main>
  )
};

export default Home;