import React, { useState } from 'react';
import { Route, Redirect } from 'react-router-dom';

const ProtectedRoute: React.FC<any> = ({ component: Component, ...rest }) => {
  const [role, setRole] = useState("none")
  return (
    <Route {...rest} render={(props:any) => (
        (role == "none")
        ?<Component {...rest} {...props} />
        :<Redirect to='/login'/>
      )
    } />
  )
}

export default ProtectedRoute;