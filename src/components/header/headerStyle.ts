import { makeStyles } from "@material-ui/core/styles";
import { Rowing } from "@material-ui/icons";

export const useStyles = makeStyles((theme) => ({
  container: {
    justifyContent: "space-between"
  },
  homeLinkSection: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    minWidth: 250
  },
  listItemContainer: {
    marginRight: 50,
    marginTop: 12
  },
  otherLinkSection: {
    display: "flex",
    flexDirection: "row",
    justifyContent: 'flex-end',
    alignItems: "center",
    minWidth: 250
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  avatarBtn: {
    display: 'flex',
    color:"#ffffff",
    '&:hover': {
      textDecoration: 'none'    
    }    
  },
  loginlogoutbtn: {
    display: 'flex',
    color:"#ffffff",
    '&:hover': {
      textDecoration: 'none'    
    }    
  },
  link: {
    color:"#ffffff",
    marginLeft: 10,
    marginRight: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 2,
    paddingBottom: 2,
    borderRadius: 6,
    '&:hover': {
      textDecoration: 'none',
      opacity: 0.8,      
      border: '1px solid #ffffff',  
    }
  },
  homeLink: {
    color:"#ffffff",
    '&:hover': {
      textDecoration: 'none'    
    }
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: 360,
  },
  lists: {
      backgroundColor: theme.palette.background.paper,
      marginTop: theme.spacing(1),
  },
  nested: {
      paddingLeft: theme.spacing(4),
  },
}));