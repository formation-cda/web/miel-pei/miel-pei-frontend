import React, { useState } from 'react'
import { CssBaseline, AppBar, Toolbar, Typography, Grid, Link, Avatar, Button, Popper, ClickAwayListener, Paper, Grow, MenuItem, MenuList } from '@material-ui/core'
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PersonIcon from '@material-ui/icons/Person';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { useStyles } from './headerStyle';
import { useMutation, useQuery } from '@apollo/client';
import { LOG_OUT_MUTATION } from '../../graphql/user/mutation/userMutation';
import {getRole, history} from '../../utils/utilities'
import { GET_ROLE_QUERY } from '../../graphql/user/query/userQuery';
import { PinDropSharp } from '@material-ui/icons';



const MenuListComposition = (props:any)=> {
    const classes = useStyles();
    const [openMenu, setOpenMenu] = React.useState(false);
    const anchorRef = React.useRef<any | null>(null);
  
    const handleToggle = () => {
      setOpenMenu((prevOpenMenu) => !prevOpenMenu);
    };
  
    const handleClose = (event:any) => {
      if (anchorRef.current && anchorRef.current.contains(event.target)) {
        return;
      }
  
      setOpenMenu(false);
    };
  
    function handleListKeyDown(event:any) {
      if (event.key === 'Tab') {
        event.preventDefault();
        setOpenMenu(false);
      }
    }
  
    // return focus to the button when we transitioned from !openMenu -> openMenu
    const prevOpenMenu = React.useRef(openMenu);
    React.useEffect(() => {
      if (prevOpenMenu.current === true && openMenu === false) {
        anchorRef.current.focus();
      }
  
      prevOpenMenu.current = openMenu;
    }, [openMenu]);
  
    return (
        <>
          <Link href="#" 
            ref={anchorRef}
            aria-controls={openMenu ? 'menu-list-grow' : undefined}
            aria-haspopup="true"
            className={classes.avatarBtn}
            onClick={handleToggle}
             >
            <Avatar alt="Cindy Baker" src="/static/images/avatar/3.jpg" />
        </Link>    
          <Popper open={openMenu} anchorEl={anchorRef.current} role={undefined} transition disablePortal  className={props.class.listItemContainer}>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={handleClose} >
                    <MenuList autoFocusItem={openMenu} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                      <MenuItem onClick={(e)=> {
                        history.push('/profile')
                        // history.go(0)
                        handleClose(e)
                      }}>Profile</MenuItem>
                      {/* <MenuItem onClick={handleClose}>My account</MenuItem> */}
                      <MenuItem onClick={(e)=> {
                        props.handlelogout()
                        handleClose(e)
                      }}>Déconnexion</MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
      </>
    );
}
  
export const Header = ()=>{
    const classes = useStyles();      
    const role = getRole()
    
    const [logout,{loading, error, data}] = useMutation(LOG_OUT_MUTATION)
    const handlelogout = ()=>{
        logout()    //  Revoquer le token au niveau du serveur
        if(data)console.log(data.logout)
        localStorage.removeItem('token')
        localStorage.removeItem('user_me')
        history.push('/')
        
        // history.go(0)
    }

    return(
        <>
        <CssBaseline />
        <AppBar position="relative">
            <Toolbar className={classes.container}>
                <div className={classes.homeLinkSection}>
                    <Link href="/" className={classes.homeLink}>
                        <HomeOutlinedIcon className={classes.icon} />
                    </Link>
                    <Typography variant="h6" color="inherit" noWrap>
                    <Link href="/" className={classes.homeLink}>
                        Miel Péi
                    </Link>
                    </Typography>
                </div >
                <Typography variant="h6" color="inherit" noWrap>
                {role === "none" &&
                    <div className={classes.otherLinkSection}>
                        <Link href="/login" className={classes.loginlogoutbtn} >                        
                            <PersonIcon className={classes.icon} />
                        </Link>
                        <Link href="/login" className={classes.loginlogoutbtn} >                        
                            Connexion
                        </Link>
                    </div>
                }
                {role === "client" &&
                    <div className={classes.otherLinkSection}>
                        <Link href="/dashboard" className={classes.link} >
                            Tableau de bord
                        </Link>   
                        <Link href="" className={classes.loginlogoutbtn} onClick={()=>{history.push('/cart')}} >                            
                            <ShoppingCartIcon className={classes.icon} />
                        </Link>
                  
                        <Link href="" className={classes.loginlogoutbtn}>                        
                            <MenuListComposition class={classes} handlelogout={handlelogout} />
                        </Link>
{/*                                     
                        <Link href="/cart" className={classes.loginlogoutbtn} >                            
                            <ExitToAppIcon className={classes.icon} />
                        </Link>      */}
                    </div>
                }
                {role === "producer" &&
                    <div className={classes.otherLinkSection}>
                        <Link href="/dashboard" className={classes.link} >
                            Tableau de bord
                        </Link>   
                        <Link href="/exploitation" className={classes.link} >
                            Exploitation
                        </Link>
                        <Link href="/product" className={classes.link} >
                            Produits
                        </Link>

                        <Link href="" className={classes.loginlogoutbtn} onClick={()=>{history.push('/cart')}} >                            
                            <ShoppingCartIcon className={classes.icon} />
                        </Link>
                  
                        <Link href="" className={classes.loginlogoutbtn}>                        
                            <MenuListComposition class={classes} handlelogout={handlelogout} />
                        </Link>
{/*                                     
                        <Link href="/cart" className={classes.loginlogoutbtn} >                            
                            <ExitToAppIcon className={classes.icon} />
                        </Link>      */}
                    </div>
                }
                {role === "admin" &&
                    <div className={classes.otherLinkSection}>
                        <Link href="/dashboard" className={classes.link} >
                            Tableau de bord
                        </Link>   
                        <Link href="/login" className={classes.link} >
                            Gestion Producteur
                        </Link>
                        <Link href="/profil" className={classes.loginlogoutbtn} >
                            <Avatar alt="Cindy Baker" src="/static/images/avatar/3.jpg" />
                        </Link>  
                        <Link href="" className={classes.loginlogoutbtn} onClick={()=>{history.push('/cart')}} >                            
                            <ShoppingCartIcon className={classes.icon} />
                        </Link>
                  
                        <Link href="" className={classes.loginlogoutbtn}>                        
                            <MenuListComposition class={classes} handlelogout={handlelogout} />
                        </Link>
{/*                                 
                        {/* <Link href="" className={classes.loginlogoutbtn} 
                            onClick={handlelogout}
                        >                            
                            <ExitToAppIcon className={classes.icon} />
                        </Link> */}
                    </div>
                }
                </Typography>
            </Toolbar>
        </AppBar>
        </>
    )
}