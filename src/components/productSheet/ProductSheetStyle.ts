import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: "85%",
  },
  image: {
    maxWidth: 500,
    minHeight: 350,
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
    borderRadius: '6px'
  },
  infos: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: "100px"
  },
  value: {
    color: '#E08610'
  }
}));