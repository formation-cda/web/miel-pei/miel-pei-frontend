
import React from 'react';
import TextField from '@material-ui/core/TextField';
import { QUERY_PRODUCT } from '../../graphql/products/query/productQuery';
import { useQuery } from '@apollo/client';
import { Container, CssBaseline, Typography, Grid, FormControl, InputLabel, Select, MenuItem, TextareaAutosize, Button, CardMedia, Card, Box, Paper, ButtonBase } from '@material-ui/core';
import { useStyles } from './ProductSheetStyle';
import { ressourcePath } from '../../utils/utilities';

const ProductSheet = (props:any) => {
  const classes = useStyles()
  const {data: dataP, loading: loadingP, error: erorP, refetch} = useQuery(QUERY_PRODUCT, {variables:{id:props.match.params.productId}})
  if(loadingP) return <p>Chargement ...</p>
  if(dataP) {
    console.log("DATAP : ",dataP)
  }
  return (
    <>
    {dataP && dataP.product && dataP.product.data &&
       <div className={classes.root}>
       <Paper className={classes.paper}>
         <Grid container spacing={2}>
           <Grid item>
             <ButtonBase className={classes.image}>
               <img className={classes.img} alt="complex" src={ressourcePath+dataP.product.data.pictures} />
             </ButtonBase>
           </Grid>
           <Grid item xs={12} sm container className={classes.infos}>
             <Grid item xs container direction="column" spacing={2}>
               <Grid item xs>
                 <Typography gutterBottom variant="h5" color='primary'>
                   {dataP.product.data.name}
                 </Typography>
                 <Typography gutterBottom variant="h6">
                   Prix: <span className={classes.value}>{dataP.product.data.price}</span>
                 </Typography>
                 <Typography gutterBottom variant="h6">
                   Quantité en stock: <span className={classes.value}>{dataP.product.data.qtyInStock}</span>
                 </Typography>
               </Grid>
               <Grid item xs>
                 <Typography gutterBottom variant="h6">
                   Description:
                 </Typography>
                 <Typography variant="body2" gutterBottom>
                   {dataP.product.data.description ? dataP.product.data.description : 'Aucune description'}
                 </Typography>
               </Grid>
             </Grid>
           </Grid>
         </Grid>
       </Paper>
     </div>
    }
    </>
  );
}
export default ProductSheet

