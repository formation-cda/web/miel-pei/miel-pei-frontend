import React, { useEffect, useState } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import {ExploitationCard} from '../card/Card'


interface IExploitationList {
    data: IExploitation[]
  }
  interface IExploitation {
    address: string,
    id: string,
    name: string,
    description:string,
    pictures: string
    lat: string,
    lng: string
  }

const Map = (props:any) => {
  const [lat, setLat] = useState(-21.115141)
  const [lng, setLng] = useState(55.536384)
  const [zoom, setZoom] = useState(10)
  const [exploitations, setExploitations] = useState <IExploitationList | null>(null)
  
  useEffect(()=>{
    setExploitations({...exploitations, data: props.exploitations?.data})
  },[props.exploitations?.data])

  return (
    <>
      <MapContainer 
        center={[lat, lng]} 
        zoom={zoom} 
        scrollWheelZoom={false}
        style={{ width: '100%', height: '500px'}}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {exploitations && exploitations.data?.length > 0 &&
            exploitations.data.map((elem:any, index:number) =>{
                return <Marker key={index} position={[Number(elem.lat), Number(elem.lng)]}>
                    <Popup>
                        <ExploitationCard data={elem} />
                    </Popup>
                </Marker>
            })
        }
      </MapContainer>
    </>
  )
};

export default Map;