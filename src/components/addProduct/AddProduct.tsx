import { useMutation, useQuery } from '@apollo/client';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControlLabel, Checkbox, Button, Link, Box, FormControl, MenuItem, Select, InputLabel, TextareaAutosize } from '@material-ui/core';
import { Copyright } from '@material-ui/icons';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import React, { useEffect, useState } from 'react'
import {useStyles} from './AddProductStyle'
import {history} from '../../utils/utilities'
import {useCommonStyles} from '../../common/commonStyle'
import PublishIcon from '@material-ui/icons/Publish';
import { ADD_PRODUCT_MMUTATION } from '../../graphql/products/mutation/productMutation';

export const AddProduct = React.forwardRef((props:any, ref:any) => {
    const classes = useStyles()
    const commonClasses = useCommonStyles()
    const [name,setName] = useState('')
    const [price,setPrice] = useState('')
    const [pictures,setPictures] = useState<any | null>()
    const [qtyInStock,setQtyInStock] = useState('')
    const [description, setDescription] = useState('');
    const [exploitation, setExploitation] = useState('');
    const [errorMessage,setErrorMessage] = useState('')
    const [message,setMessage] = useState({
        success: null,
        message: ""
    })

    const handleChange = (event:any) => {
        setExploitation(event.target.value);
    };

    const [addProduct, {loading, error, data}] = useMutation(ADD_PRODUCT_MMUTATION,
        {
            onCompleted(data) {
                console.log("DATA :",data)
              if (data) {
                setTimeout(()=>{
                    setMessage({...message, success: data?.addProduct.success, message: data?.addProduct.message})
                },3000)
              }
              if (data && props.handleClose) {
                setTimeout(()=>{   
                    if(props.handleRefetch){
                        props.handleRefetch()
                    }
                    props.handleClose()
                },6000)
              }
            }
          })
          
    return (
            <Container component="main" maxWidth="xs" className={classes.main} ref={ref}>
                <CssBaseline />
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Ajout de produits
                    </Typography>
                    <form className={classes.form} 
                        onSubmit={async e => {
                            e.preventDefault();
                            //  appel de la methode de mutation
                            addProduct({
                                variables:{
                                    name: name,
                                    price: price,
                                    pictures: pictures,
                                    qty_in_stock: qtyInStock,
                                    description: description,
                                    exploitationId: exploitation
                                }
                            })
                        }}
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={12}>
                             <FormControl variant="outlined" className={classes.formControl}>
                                <InputLabel id="demo-simple-select-outlined-label">Exploitation</InputLabel>
                                <Select
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    value={exploitation}
                                    onChange={(e)=> {handleChange(e)}}
                                    label="Exploitation"
                                >
                                    <MenuItem value="">
                                        <em>Aucune</em>
                                    </MenuItem>
                                    {props.exploitations && props.exploitations.data?.length > 0&&
                                        props.exploitations.data.map((elem:any, index: number)=>{
                                           return <MenuItem value={elem.id}>{elem.name}</MenuItem>
                                        })
                                    }
                                </Select>
                            </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <TextField
                                    autoComplete="name"
                                    name="name"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Nom"
                                    onChange={
                                        (e)=>{
                                            setName(e.target.value)
                                        }
                                    }
                                    autoFocus
                                />                            
                            </Grid>
                            <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="price"
                                label="Prix"
                                name="price"
                                onChange={
                                    (e)=>{
                                        setPrice(e.target.value)
                                    }
                                }
                                autoComplete="prix"
                            />
                            </Grid>
                            <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="qty_in_stock"
                                label="Quantité"
                                name="qty_in_stock"
                                onChange={
                                    (e)=>{
                                        setQtyInStock(e.target.value)
                                    }
                                }
                                autoComplete="qty_in_stock"
                            />
                            </Grid>
                            <Grid item xs={12}>
                                <TextareaAutosize 
                                    className={classes.textArea} 
                                    aria-label="minimum height" 
                                    rowsMin={3} 
                                    placeholder="Déscription" 
                                    onChange={
                                        (e)=>{
                                            setDescription(e.target.value)
                                        }
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} sm={12}>

                            <div className={classes.upload_contaier}>
                                <label className="customFileUpload">
                                <input type="file" 
                                onChange={(e)=>{
                                    const file: any = e.target.files;
                                    setPictures(file[0])
                                }} />
                                {/* <i className="fa fa-cloud-upload" /> Attach */}

                                 <div className={classes.upload}><PublishIcon /> Upload</div>
                                </label> 
                                <div>{pictures?.name}</div>
                            </div>
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit+" "+commonClasses.button}
                        >
                            Valider
                        </Button>
                        <Grid container justify="center">
                            <Typography>
                                {errorMessage !== "" && <p className={classes.error}>{errorMessage}</p>}
                                {message.success === false &&
                                        <p className={classes.error}>{message.message}</p>
                                }
                                {message.success &&                                    
                                    <p className={classes.success}>{message.message}</p> 
                                }
                            </Typography>
                        </Grid>
                    </form>
                </div>
            </Container>
        )    
    }
)    