import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({    
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  expCard: {
    height: '100%',
    width: "200px",
    display: 'flex',
    flexDirection: 'column',
    boxShadow: 'none'
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  expCardContent: {
    flexGrow: 1,
    paddingLeft: 0,
    paddingBottom: 0,
    paddingRight: 0
  },
  expCardName: {
    justifyContent: 'center',
    textAlign: 'center'
  },
  customBox: {
    display: "-webkit-box",
    boxOrient: "vertical",
    lineClamp: 2,
    wordBreak: "break-all",
    overflow: "hidden"
  },
  priceContainer: {
    display:'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color:"#ffffff",
    backgroundColor: "#E0860F",
    margin: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 6,
    '&:hover': {
      textDecoration: 'none',
      opacity: 0.8,      
      border: '1px solid #E0860F',  
      backgroundColor: "#ffffff",
      color:"#E0860F",
    }
  },
  priceContent: {
    margin: 0
  }
}));