import React from "react"
import { CssBaseline, AppBar, Toolbar, Typography, Container, Grid, Button, Card, CardMedia, CardContent, CardActions } from '@material-ui/core';
import { ressourcePath } from "../../utils/utilities"
import {useStyles} from './CardStyle'
import {history} from '../../utils/utilities'

export const ProductCard = (props: any) => {
    const classes = useStyles();
    return (
        <Card className={classes.card}>
        <CardMedia
          className={classes.cardMedia}
          image={ressourcePath+props.data.pictures}
          title="Image title"
        />
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
            {props.data.name}
          </Typography>
          <Typography className={classes.customBox}>
            {props.data.description}
          </Typography>
        </CardContent>
        <CardActions className={classes.priceContainer}>
          <Typography gutterBottom variant="h5" component="h2" className={classes.priceContent}>
            {props.data.price}€
          </Typography>
        </CardActions>        
        <CardActions>
          <Button size="small" color="primary"  onClick={()=>{
            history.push('/product-sheet/'+props.data.id)
            history.go(0)
          }}>
            Voir plus
          </Button>
          <Button size="small" color="primary">
            Ajouter au panier
          </Button>
        </CardActions>
      </Card>  
    )
}

export const ExploitationCard = (props: any) => {
    const classes = useStyles();
    return (
      <Card className={classes.expCard}>
        <CardMedia
          className={classes.cardMedia}
          image={ressourcePath+props.data.pictures}
          title="Image title"
        />
        <CardContent className={classes.expCardContent}>
          <Typography gutterBottom variant="h5" component="h2" className={classes.expCardName}>
            {props.data.name}
          </Typography>
          <Typography className={classes.customBox}>
            {props.data.description}
          </Typography>
        </CardContent>
        {/* <CardActions className={classes.priceContainer}>
          <Typography gutterBottom variant="h5" component="h2" className={classes.priceContent}>
            {props.data.price}€
          </Typography>
        </CardActions>         */}
        <CardActions style={{justifyContent:'center'}}>
          <Button size="small" color="primary" onClick={()=>{
            history.push('/exploitation-sheet/'+props.data.id)
            history.go(0)
          }}>
            Voir plus
          </Button>
          {/* <Button size="small" color="primary">
            Ajouter au panier
          </Button> */}
        </CardActions>
      </Card>  
    )
}

