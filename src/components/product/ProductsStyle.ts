import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    large: {
      width: theme.spacing(10),
      height: theme.spacing(8),
      marginRight: 15,
      marginTop: 10,
      marginBottom: 10
    },
    customBox: {
      display: "-webkit-box",
      boxOrient: "vertical",
      lineClamp: 2,
      wordBreak: "break-all",
      overflow: "hidden"
    },
    title: {
      textAlign:'center',
      margin: 20
    },
    'container': {
      width: '100%',
    },
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    textArea: {
      width: '100%',
      padding: 8
    },
    upload: {
      display: 'flex',
      textAlign: 'center',
      alignItems: 'center',
      border: '1px solid grey',
      padding: '5px',
      borderRadius: 3,
      marginRight: 5
    },
    upload_contaier: {
      fontFamily: 'sans-serif',
      display: 'flex',
      textAlign: 'center',
      alignItems: 'center',
      '& input[type="file"]': {
        display: 'none'
      }
    },
    customFileUpload: {
        border: '1px solid #000',
        backgroundColor: 'red',
        display: 'inline-block',
        padding: '6px 12px',
        cursor: 'pointer',
    },    
    filePreview: {
      margin: '0 10px',
      backgroundColor: 'red'
    },
    formControl: {
      minWidth: 120,

      width: '100%', // Fix IE 11 issue.
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    error: {
      color:"red",
    },
    success: {
      color:"green",
    },
  }));