import { useMutation, useQuery } from '@apollo/client';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControlLabel, Checkbox, Button, Link, Box, FormControl, MenuItem, Select, InputLabel, TextareaAutosize, Divider, Modal, IconButton } from '@material-ui/core';
import { Copyright } from '@material-ui/icons';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import React, { Fragment, useEffect, useState } from 'react'
import {useStyles} from './ProductsStyle'
import {history} from '../../utils/utilities'
import {useCommonStyles} from '../../common/commonStyle'
import PublishIcon from '@material-ui/icons/Publish';
import { ADD_PRODUCT_MMUTATION, DELETE_PRODUCT_MMUTATION } from '../../graphql/products/mutation/productMutation';
import { QUERY_MY_EXPLOITATIONS } from '../../graphql/exploitation/query/exploitationQuery';
import { ressourcePath } from "../../utils/utilities"

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/Delete';
import UnfoldMoreIcon from '@material-ui/icons/UnfoldMore';
import CachedIcon from '@material-ui/icons/Cached';
import AddIcon from '@material-ui/icons/Add';
import classes from '*.module.css';
import { AddProduct } from '../addProduct/AddProduct';
import { QUERY_PRODUCTS } from '../../graphql/products/query/productQuery';

interface IExploitationList {
    data: IExploitation[]
}
interface IExploitation {
    address: string,
    id: string,
    name: string,
    description:string,
    pictures: string
    lat: string,
    lng: string
}

const ProductItem = (props:any) => {
    return <Fragment  key={props.itemKey}>
                <ListItem alignItems="center" >
                {/* <ListItemAvatar> */}
                <Avatar  
                    variant="rounded"
                    alt={props.product.name}
                    src={ressourcePath+props.product.pictures} 
                    className={props.class.large}
                />
                {/* </ListItemAvatar> */}
                {/* <div style={{ alignItems: "center"}} > */}
                    <ListItemText
                        primary={props.product.name}
                        secondary={
                            <div className={props.class.customBox}>
                                {props.product.description}
                            </div>
                        }
                    />
                {/* </div> */}
                <IconButton aria-label="delete">
                        <UnfoldMoreIcon color='primary' />
                </IconButton>
                <IconButton aria-label="delete">
                        <CachedIcon style={{ color: "blue"}} />
                </IconButton>
                <IconButton aria-label="delete" onClick={()=>{props.handleDelete(props.product.id)}}>
                        <DeleteIcon style={{ color: "red"}} />
                </IconButton>
            </ListItem>
            <Divider variant="inset" component="li" style={{marginLeft: 0}}/>
        </Fragment>
}

export const Products = () => {
    const classes = useStyles()
    const commonClasses = useCommonStyles()
    const [exploitationsList, setExploitationsList] = useState <IExploitationList | null>(null);
    const [update, setUpdate] = React.useState(false);  
    const [open, setOpen] = React.useState(false);  
    const {data: dataE, loading: loadingE, error: erorE} = useQuery(QUERY_MY_EXPLOITATIONS)
    const {data: dataP, loading: loadingP, error: erorP, refetch} = useQuery(QUERY_PRODUCTS)
    const [deleteProduct, {loading, error, data}] = useMutation(DELETE_PRODUCT_MMUTATION,
        {
            onCompleted(data) {
                console.log("DATAAAA", data)               
                handleRefetch()
            }
          }
        )
    
    const handleOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const handleRefetch = () => {
        refetch()
    };

    const handleDelete = (id:string) => {
        deleteProduct({ variables: { id } })
    }

    const handleLoadExploitation = async (dataE:any) => {
        //    await setTimeout(async ()=>{
            await setExploitationsList({...exploitationsList, data: dataE?.myExploitations?.data})           
        // },3000)
        console.log("ExploitationList  DATAE:",dataE)
        console.log("ExploitationList :",exploitationsList)
    };

    useEffect(()=>{
        if(dataE) {
            handleLoadExploitation(dataE)
        }
        console.log("refetch")
    },[dataP, dataE])

    if(dataP) {
        console.log('DATA P :',dataP)
    }
    return (
            <Container component="main">
                <Typography component="h1" variant="h4" className={classes.title}>
                    Liste des produits
                </Typography>
                <div className="fab-container">
                    <Fab 
                        color="primary"
                        aria-label="add"
                        size="small"
                        onClick={()=>{
                            handleOpen()
                        }}
                    >
                        <AddIcon />
                    </Fab>
                </div>
                <div className={classes.paper}>
                    {dataP && dataP.products?.data && dataP.products?.data.length > 0 &&
                        <List dense className={classes.root}>
                            {dataP && dataP.products.data.length > 0 &&
                                dataP.products.data.map((product:any,index:number) => <ProductItem itemKey={index} class={classes} product={product} handleDelete={handleDelete} />)                        
                            }
                        </List>                        
                    }
                    {!dataP || (dataP && dataP.products.data && dataP.products.data.length < 1) &&
                        <Typography component="h1" variant="h5">
                            Aucun produits
                        </Typography>
                    }                    
                </div>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    style={{display:'flex', alignItems: 'center'}}
                >
                    <AddProduct handleClose={handleClose} exploitations={exploitationsList} handleRefetch={handleRefetch} />
                </Modal>
            </Container>
        )        
}